#include <stdio.h>
#include <stdlib.h>

// Recursive function, low and high are array indices
void stoogeSort(int arr[], const int low, const int high){
	
	int tmp,k;
	
	if(arr[low]>arr[high]){
		tmp = arr[low];
		arr[low]=arr[high];
		arr[high]=tmp;
	}
	if((low+1)>=high){
		return;
	}
	k=(int)((high-low+1)/3);
	
	stoogeSort(arr,low,high-low);
	stoogeSort(arr,low+high,high);
	stoogeSort(arr,low,high-low);
	
}

int main(){
	int i;
	int X[6] = {15,7,32,8,4,6};
	stoogeSort(X,0,5);
	for(i=0;i<6;i++){
		printf("%d",X[i]);
	}
	return 0;
}


